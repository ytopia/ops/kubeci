#!/bin/bash
set -e

# required vars
declare -a required_vars=(
  KUBECI_K8S_DEPLOY_SERVER \
  KUBECI_K8S_DEPLOY_NS \
  KUBECI_HELM_CHART \
  KUBECI_HELM_RELEASE \
  KUBECI_IMAGE_TAG \
)
for var_name in "${required_vars[@]}"
do
  if [ -z "$(eval "echo \$$var_name")" ]; then
    echo "Missing environment variable $var_name"
    exit 1
  fi
done


export KUBECI_K8S_TOKEN_SECRET_NAME=${KUBECI_K8S_TOKEN_SECRET_NAME:-"k8s"}
export KUBECI_K8S_TOKEN_SECRET_KEY=${KUBECI_K8S_TOKEN_SECRET_KEY:-"token"}
export KUBECI_HELM_ARGS=${KUBECI_HELM_ARGS:-""}

cat $(dirname $0)/deploy-job.yaml | gomplate | kubectl -n $KUBECI_K8S_NS create -f -
